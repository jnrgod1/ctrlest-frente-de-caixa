<?php

    session_start();

    if(isset($_POST['busca']) && $_POST['busca'] == 'sim') {
        include_once "../connection.php";
        $textoBusca = strip_tags($_POST['texto']);
        $buscar = $pdo->prepare("SELECT * FROM `produto` WHERE `nome_prod` LIKE '%$textoBusca%'");
        $buscar->execute();

        $retorno = array();
        $retorno['dados'] = '';
        $retorno['qtd'] = $buscar->rowCount();
        if($retorno['qtd'] >= 0) {
            while($conteudo = $buscar->fetchObject()){
               $retorno['dados'] .= '<a href="#" id="'.$conteudo->id_prod.':'.$conteudo->preco_venda.'">'.utf8_encode($conteudo->nome_prod).'</a>';
            }
        }

        echo json_encode($retorno);
    }

    if(isset($_POST['add_produto'])) {
        include_once "../connection.php";

        $retorno = array();
        $retorno['dados'] = '';

        $produtoId = (int)$_POST['produto'];

        if(isset($_SESSION['carrinho'][$produtoId])) {
            $_SESSION['carrinho'][$produtoId] += 1;
        } else {
            $_SESSION['carrinho'][$produtoId] = 1;
        }

        $total = 0;

        foreach($_SESSION['carrinho'] as $idProd => $qtd) {
            $pegaProduto = $pdo->prepare("SELECT * FROM `produto` WHERE `id_prod` = ?");
            $pegaProduto->execute(array($idProd));
            $dadosProduto = $pegaProduto->fetchObject();

            $subtotal = ($dadosProduto->preco_venda*$qtd);
            $total += $subtotal;

            $retorno['dados'] .= '<tr><td>'.utf8_encode($dadosProduto->nome_prod).'</td><td>R$ '.$dadosProduto->preco_venda.'</td> <td><input type="text" value="'.$qtd.'" id="qtd"></td>';
            $retorno['dados'] .= '<td>R$ '.number_format($subtotal, 2, ',','.').'</td></tr>';
        }

        $retorno['dados'] .= '<tr><td colspan="3">Total</td><td id="total" style="color: green;">R$ '.number_format($total, 2, ',','.').'</td></tr>';

        echo json_encode($retorno);
    }

?>