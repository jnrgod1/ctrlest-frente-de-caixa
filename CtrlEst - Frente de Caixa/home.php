<?php

    session_start();
    session_destroy();


?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CtrlEst - Operacional</title>
    <link rel="stylesheet" href="css/all.css">
    <link rel="stylesheet" href="css/styles.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head>

<body>

    <div class="container active" id="container">

        <header class="header">
            <div class="container-logo">
                <button class="btn-menu" id="btn-menu"><i class="fas fa-bars"></i></button>
                <a href="#" class="logo-header"><img src="assets/img/ctrlest-logo.svg" alt=""></a>
            </div>

            <div class="info-header">
                <span>Operador: Júnior Albuquerque</span>
                <button><i class="fas fa-power-off"></i></button>
            </div>
        </header>

        <nav class="sidebar">

            <a href="#" class="active"><i class="fas fa-shopping-cart"></i>Realizar Venda</a>
            <a href="#"><i class="fas fa-user-plus"></i>Cadastrar Cliente</a>
            <a href="#"><i class="far fa-handshake"></i>Realizar Orçamento</a>

            <hr>

            <a href="#"><i class="fas fa-box-open"></i>Estoque</a>
            <a href="#"><i class="fas fa-address-card"></i>Consultar Cliente</a>
            <a href="#"><i class="fas fa-power-off"></i>Sair</a>

        </nav>

        <main class="main">
            <div class="selection">
                <div class="produto">
                    <!-- <form action="" method="POST"> -->
                        <label for="">Produto</label>
                        <input type="text" name="buscar" id="busca">
                    <!-- </form> -->
                </div>

                <div id="result-busca" class="result-busca"></div>

                <!-- <div class="qtd">
                    <label for="">Quantidade</label>
                    <input type="number" name="" id="">
                </div>

                <div class="add">
                    <label for="">Adicionar a lista</label>
                    <button><i class="fas fa-plus-circle"></i></button>
                </div> -->
            </div>

            <div class="lista">

                <div class="lista-produtos">
                    <label for="">Lista de Compra</label>

                    <form action="">
                        <table cellpadding="0" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Produto</th>
                                    <th>Valor</th>
                                    <th>Qtd</th>
                                    <th>Subtotal</th>
                                </tr>
                            </thead>

                            <tbody id="return">
                                <!-- <div id="return" class="return"> -->
                                    <!-- <tr>
                                        <td>Camisa Star Wars Darth Vader</td>
                                        <td>R$ 29,90</td>
                                        <td><input type="number" value="3" id="qtd"></td>
                                        <td>R$ 50,00</td>
                                    </tr>

                                    <tr>
                                        <td colspan="3">Total</td>
                                        <td id="total" style="color: green;">R$ 100,00</td>
                                    </tr> -->
                                <!-- </div> -->
                            </tbody>
                        </table>
                    </form>
                </div>

                <div class="venda">
                <!-- <form action=""> -->
                    <label for="">Data de saída</label>
                    <input type="date" required name="" id="">
                    <a href=""><button type="submit">Efevitar Venda</button></a>
                    <!-- </form> -->
                </div>
            </div>
        </main>
    </div>

    <script src="js/functions.js"></script>
</body>
</html>