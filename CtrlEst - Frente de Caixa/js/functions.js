$(function () {
    $('#busca').keyup(function () {
        var buscaTexto = $(this).val();
        if (buscaTexto.length >= 3) {
            $.ajax({
                method: 'post',
                url: 'sys/sys.php',
                data: { busca: 'sim', texto: buscaTexto },
                dataType: 'json',
                success: function (retorno) {
                    if (retorno.qtd == 0) {
                        $('#result-busca').html('<a style="color: red;">Não achei nada véi :(</a>');
                    } else {
                        $('#result-busca').html(retorno.dados);
                    }
                }
            });
        };

        if (buscaTexto.length == 0) {
            $('#result-busca').html('');
        }
    });

    $('body').on('click', '#result-busca a', function () {
        var dadosProduto = $(this).attr('id');
        var splitDados = dadosProduto.split(':');

        $.ajax({
            method: 'post',
            url: 'sys/sys.php',
            data: { add_produto: 'sim', produto: splitDados[0] },
            dataType: 'json',
            success: function (retorno) {
                $('tbody#return').html(retorno.dados);
            }
        })
    });
})
